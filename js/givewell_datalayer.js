(function ($) {
    Array.prototype.slice.call(document.querySelectorAll("[required]")).forEach(function(input){
        input.addEventListener('invalid',function(e){
            dataLayer.push({'event': 'error', 'html_validation_error' : input.id + ' : ' + input.value});
        })
    });
})();
